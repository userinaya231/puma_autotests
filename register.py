#coding: utf-8
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager


class registerProfile(unittest.TestCase):
    def test_profile(self):
        driver = webdriver.Chrome(service=ChromeService(executable_path=ChromeDriverManager().install()))
        driver.get("https://us.puma.com/")
        driver.implicitly_wait(10)
        try:
            sign_in_button = driver.find_element(by=By.XPATH, value='//a[.="Sign in"]')
            sign_in_button.click()
            login_page = driver.find_element(by=By.LINK_TEXT, value='Create account')
            register_text = login_page.text
            assert register_text == 'Create account'

            login_page.click()

            driver.current_url == 'https://us.puma.com/us/en/account/login?action=register&from=cart'

            input_firstName = driver.find_element(by=By.NAME, value='firstName')
            input_lastName = driver.find_element(by=By.NAME, value='lastName')
            input_email = driver.find_element(by=By.NAME, value='email')
            input_password = driver.find_element(by=By.NAME, value='password')
            button_submit = driver.find_element(by=By.XPATH,
                                                value="//button[@type='submit']//span//div[.='Register']")

            input_firstName.send_keys("Andrew")
            input_lastName.send_keys("Frolov")
            input_email.send_keys("d34d1574nd+14@mail.ru")
            input_password.send_keys('12345678')
            button_submit.is_enabled()
            button_submit.click()

            driver.current_url == 'https://us.puma.com/us/en/cart'
            header_card = driver.find_element(by=By.XPATH, value="//p[text()='Your Shopping Cart is Empty']")
            header_text = header_card.text
            assert header_text == 'Your Shopping Cart is Empty'
        finally:
            driver.quit()

if __name__ == "__main__":
    unittest.main()