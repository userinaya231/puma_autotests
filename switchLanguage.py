#coding: utf-8
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager


class switchLanguage(unittest.TestCase):
    def test_language(self):
        driver = webdriver.Chrome(service=ChromeService(executable_path=ChromeDriverManager().install()))
        driver.get("https://us.puma.com/")
        driver.implicitly_wait(10)
        try:
            menu_btn = driver.find_element(by=By.XPATH, value='//button[@data-test-id="account-button"]')
            menu_btn.click()

            language_button = driver.find_element(by=By.XPATH, value='//button[@type="button"]//span[text()="Language"]')
            language_button.click()

            lang_btn_click = driver.find_element(by=By.XPATH, value='//li[@lang="pl"]')
            lang_btn_click.click()

            assert driver.current_url == 'https://eu.puma.com/pl/pl/home'
        finally:
            driver.quit()


if __name__ == "__main__":
    unittest.main()