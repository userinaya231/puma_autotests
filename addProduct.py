#coding: utf-8
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager


class addProduct(unittest.TestCase):
    def test_add_product_in_cart(self):
        driver = webdriver.Chrome(service=ChromeService(executable_path=ChromeDriverManager().install()))
        driver.get("https://us.puma.com/")
        driver.implicitly_wait(10)
        try:
            driver.execute_script("window.scrollTo(0, 1150);")
            cart_product = driver.find_element(by=By.XPATH, value='//h3[@title="RS-X Taped Sneakers"][.="RS-X Taped Sneakers"]')
            cart_product.click()

            header_product = driver.find_element(by=By.XPATH, value='//h1[@data-test-id="pdp-title"][.="RS-X Taped Sneakers"]')
            text_product = header_product.text
            assert text_product == 'RS-X Taped Sneakers'

            driver.execute_script("window.scrollTo(0, 200)")
            size_product = driver.find_element(by=By.XPATH, value='//span[@data-content="size-value"][.="7"]')
            size_product.click()

            quantity_product_btn = driver.find_element(by=By.XPATH, value='//div[@data-test-id="pdp-quantity-select-wrapper"]')
            quantity_product_btn.click()

            quantity_select = driver.find_element(by=By.XPATH, value='//option[@value=4]')
            quantity_select.click()

            add_to_cart_btn = driver.find_element(by=By.XPATH, value='//button[@aria-label="Add to Cart"]')
            add_to_cart_btn.click()

            added_to_cart = driver.find_element(by=By.XPATH, value='//h2[text()="Added to cart"]')
            added_to_cart_text = added_to_cart.text
            assert added_to_cart_text == 'Added to cart'
        finally:
            driver.quit()

if __name__ == "__main__":
    unittest.main()