#coding: utf-8
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager


class searchProduct(unittest.TestCase):
    def test_profile(self):
        driver = webdriver.Chrome(service=ChromeService(executable_path=ChromeDriverManager().install()))
        driver.get("https://us.puma.com/")
        driver.implicitly_wait(10)
        try:
            search_input = driver.find_element(by=By.XPATH, value='//input[@type="search"]')
            search_input.send_keys("MINI PUFFY SLIPPERS\n")
            search_input.click()
            time.sleep(5)
            driver.execute_script("window.scrollTo(0, 500)")
            time.sleep(5)

            product_name = driver.find_element(by=By.XPATH,
                                               value='//a[@aria-label="Go to PUMA x PERKS AND MINI Puffy Slippers Puma Black-Heliotrope"]//h3[text()="PUMA x PERKS AND MINI Puffy Slippers"]')
            product_name.click()

            product_page_name = driver.find_element(by=By.XPATH, value='//h1[@id="pdp-product-title"]')
            product_page_head = product_page_name.text
            assert product_page_head == 'PUMA x PERKS AND MINI Puffy Slippers'
        finally:
            driver.quit()


if __name__ == "__main__":
    unittest.main()