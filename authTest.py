#coding: utf-8
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager


class authTest(unittest.TestCase):
    def test_profile(self):
        driver = webdriver.Chrome(service=ChromeService(executable_path=ChromeDriverManager().install()))
        driver.get("https://us.puma.com/us/en/cart")
        driver.implicitly_wait(10)
        try:
            sign_in_button = driver.find_element(by=By.XPATH, value='//a[.="Sign in"]')
            sign_in_button.click()

            login_page = driver.find_element(by=By.LINK_TEXT, value='Create account')
            login_text = login_page.text
            assert login_text == 'Create account'

            driver.current_url == 'https://us.puma.com/us/en/account/login?from=cart'

            input_email = driver.find_element(by=By.NAME, value='email')
            input_password = driver.find_element(by=By.NAME, value='password')
            button_submit = driver.find_element(by=By.XPATH, value="//button[@type='submit']//span//div[.='Login']")

            input_email.send_keys("d34d1574nd+1@mail.ru")
            input_password.send_keys('12345678')
            button_submit.is_enabled()
            button_submit.click()

            driver.current_url == 'https://us.puma.com/us/en/cart'
            header_card = driver.find_element(by=By.XPATH, value="//h1[@id='section-cart-title'][text()='My Shopping Cart']")
            header_text = header_card.text
            assert header_text == 'My Shopping Cart'
        finally:
            driver.quit()

if __name__ == "__main__":
    unittest.main()



